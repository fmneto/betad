#ifndef FILEUTILS_INCLUDED
#define FILEUTILS_INCLUDED
#endif

#include <stdio.h>
#include <stdlib.h>

int file_lines (FILE *file);
double **file_read (char *fname, int *n);
FILE *file_bjh_output (char *orig);
double **get_array(int x1, int x2);      // Allocates an array ox x1 rows and x2 cols

