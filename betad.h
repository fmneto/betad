#define MAX_NAME_SZ 256
#define MAX_LINE_SIZE 256

#define N_AVOGADRO 6.022e23    // Avogadro's constant
#define N2_CROSS_SECTION 0.164e-18   // Cross-section of N2
#define MOLAR_VOLUME 22.4e3  // Molar volume of gas

