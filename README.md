###
# BETter ADsorption
#
###

This program is an attempt at making adsorption isotherm analysis
simpler. It intends to implement procedures for analysis of adsorption
results, such as surface area and pore side distribution
calculations.

It is an open source initiative and therefore is released under the GPL.

