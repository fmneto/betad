#ifndef ADS_MATH_INCLUDED
#define ADS_MATH_INCLUDED
#endif

// Adsorption specific functions

double harkins_jura (double x);
double halsey (double x);
double kjs (double x);
double kjs_r (double x);
double kelvin (double x);
int run_bet (double **data, int lines);  // Runs BET method on data
int run_bjh (double **data, int lines, char *fname);  // Runs BJH method on data
