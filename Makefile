CC = gcc
CFLAGS = -g -I. -I/usr/include/gsl

DEPS = file_util.h
OBJ = betad.o file_util.o ads_math.o

LIBS = -lm -lgsl -lgslcblas

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

betad: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

clean:
	rm -f *.o *~ core 



