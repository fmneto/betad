#include <string.h>
#include "betad.h"
#include "file_util.h"


// General utility functions

int file_lines (FILE *file) {  // Counts the lines in a text file

  int i=0, c;

  while (!feof(file)) {
    c = fgetc(file);
    if (c == '\n')
      i++;
    else if (c == '#')
      i--;
  }

  rewind(file);
  return i;
  
}

double **file_read (char *fname, int *n) {

  FILE *input;
  int i, lines, c;
  char *line;

  double **data;
  
  printf ("Loading file: %s\n", fname);

  if (!(input=fopen(fname, "r")))
    exit(1);

  lines = file_lines(input);
  *n = lines;
  
  if(!(data = malloc(2 * sizeof(double *))))
    exit(1);
  for (i=0; i<2; i++)
    if(!(data[i] = malloc((*n) * sizeof(double))))
      exit(1);

  if(!(line = malloc (MAX_LINE_SIZE*sizeof(char))))
    exit(1);

  for (i=0; i<(*n); i++) {
    fgets(line, MAX_LINE_SIZE, input);
    if (line[0] == '#') continue;
    line[strlen (line) - 1] = '\0';
    data[0][i] = atof(strtok(line, " \t"));
    data[1][i] = atof(strtok(NULL, " \t"));
  }


  fclose(input);
  free(line);
  return data;

  
}

FILE *file_bjh_output (char *orig) {

  char *new_name;
  FILE *bjh_file;

  size_t name_l = strlen (orig);

  if(!(new_name = malloc((name_l+5)*sizeof(char)))) // +4 = ".bjh" + '\0'
    exit(1);

  printf("Original file name: %s\n", orig);
  
  memcpy (new_name, orig, name_l);
  memcpy (new_name+name_l, ".bjh", 5);

  if (!(bjh_file=fopen(new_name, "w")))
    exit(1);

  printf("Opening file %s for writing...\n", new_name);
  
  free(new_name);

  return bjh_file;
  
}

double **get_array(int x1, int x2) {    // Allocates an array ox x1 rows and x2 cols

  double **array;
  int i;

  if(!(array = malloc(x1 * sizeof(double))))
    exit(1);
  for (i=0; i<x1; i++)
    if(!(array[i] = malloc(x2 * sizeof(double))))
      exit(1);

  return array;

}

