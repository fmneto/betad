#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <gsl_fit.h>
#include "file_util.h"
#include "ads_math.h"
#include "betad.h"

int main (int argc, char *argv[]) {

  int lines, i, j;
  char *name;
  double **data;
  double S_bet;

  if (!(name = malloc(sizeof(char)*MAX_NAME_SZ))) {
    printf("No memory!\n");
    return 1;
  }

  if (argc == 1) {      // No extra arguments on the command line
    printf("Name of the input file: ");
    fgets(name, MAX_NAME_SZ, stdin);
  } else if (argc == 2) {    // Command line argument should be the input file, if any.
    strcpy(name, argv[1]);
  }
  else {
    printf ("Too many arguments! Exiting.\n");
    exit(1);
  }
  if ((strlen(name) > 0) && (name[strlen (name) - 1] == '\n'))
    name[strlen (name) - 1] = '\0';
  
  data = file_read (name, &lines);    // File loaded into data[][].   
  
  if (!(S_bet = run_bet(data, lines)))     // Run BET, or exit(1) if it fails.
    exit(1);

  printf ("S_bet = %3.3lf m²/g\n", S_bet);

  if (!(run_bjh(data, lines, name)))     // Same for BJH
    exit(1);
  
  free (data);
  return 0;
  
}

