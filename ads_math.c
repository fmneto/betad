#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <gsl_fit.h>
#include "file_util.h"
#include "betad.h"
// Adsorption-related math functions

double harkins_jura (double x) {

  return 0.1*pow(60.65/(0.03071-log10(x)), 0.3968);
  
}


double halsey (double x) {

  return 0.354*pow(5/(-log(x)), 0.3333);
  
}


double kjs (double x) {  // "Complete" KJS equation

  return pow((60.65/(0.03071-log10(x))), 0.3968);
  
}

double kjs_r (double x) { // KJS version of the Kelvin equation - radius
  
  return -0.575/log10(0.875*x);

}

double kelvin (double x) {

  return 0.415/(0-log10(x));

}

int run_bet (double **data, int lines) {

  int i, j, n0, n;
  double S_bet;
  double **bet;
  double a, b, cov00, cov01, cov11, chisq; // Fit variables
  int status;

  size_t size_bet;

  n=0;                        // Start point of the BET region
  n0=0;                       // End point of the BET region
  for (i=0; i<lines; i++)                    
    if (data[0][i] >= 0.05 && data[0][i] < 0.3) {           // Find data points in BET range
      n++;
      if (n0 == 0)
	n0 = i;
    }
  
  if(!(bet = malloc(3 * sizeof(double *))))
    return 1;
  for (i=0; i<=2; i++)
    if(!(bet[i] = malloc(n * sizeof(double))))
      return 1;

  for (i=0; i<n; i++) {
      bet[0][i] = data[0][i+n0];
      bet[1][i] = 1/(data[1][i+n0]*((1/data[0][i+n0])-1));  // y' = 1/(y*(x-1))
      bet[2][i] = 1/(bet[1][i]*0.05);                 // weight for the fits, based on 5% estimated error
  }

  // Linear fit to BET-transformed data from 0.05 < x < 0.3 (y' = a + bx')

  printf ("Performing fit with %d points...", n);
  status = gsl_fit_wlinear (bet[0], 1, bet[2], 1, bet[1], 1, n, &a, &b, &cov00, &cov01, &cov11, &chisq);
  if (status) printf ("not ");
  printf ("done! (status = %d)\n", status);

  printf ("====== BET results ======\n");
  printf ("y = %.3e + %.3e x\n", a, b);
  printf ("[%.3e, %.3e\n %.3e, %.3e]\n", 
	  cov00, cov01, cov01, cov11);
  printf ("Chi Square = %.3e\n", chisq);
  printf ("=========================\n");


  size_bet = 0;
  for (i=0; i<3; i++)
    for (j=0; j<n; j++)
      size_bet += sizeof(bet[i][j]);

  for (i=0; i<2; i++)
    for (j=0; j<lines; j++)
      size_bet += sizeof(data[i][j]);

  if (size_bet < 1024)
    printf ("%zu bytes used.\n", size_bet);
  else
    printf ("%.2zf kbytes used.\n", (double) size_bet/1024);

  S_bet = 1/(a+b);
  S_bet *= N_AVOGADRO;
  S_bet *= N2_CROSS_SECTION;
  S_bet /= MOLAR_VOLUME;  

  free(bet);
  return S_bet;
  
}

int run_bjh (double **data, int lines, char *fname) {

  int i, j;
  int n, n0;
  int ads;

  double v_p;
  
  double **bjh;
  double **bjh_r, **bjh_m;
  double **psd;

  FILE *bjh_output;

  /* bjh_r is the matrix with values for r_k, t and r_p calculated from p/p0
     while bjh_m is the calculation matrix for the BJH method. psd is the 
     matrix that will store final results. */

  n0=0;
  for (i=1; i<lines; i++) {

    if (data[0][i] < data[0][i-1]) {  // Figure out number of points in the desorption branch
      n0 = i-1;                       // n0 is the position of the first point in the branch
      printf ("p/p0 máximo: %.3f\n", n0, data[0][i-1], data[1][i-1]);
      break;
    }
    
  }

  if (n0 == 0) {
    ads = 1;                // There is only adsorption data, so let's use them
    n = lines;
  }
  else {
    ads = 1;                // Desorption data available
    n = n0;
  }                         // n is the number of points to be used.

  
  bjh = get_array(2, n);

  if (ads == 1) {
    for (i=0; i<n; i++) {               
      bjh[0][i] = data[0][n-i];
      bjh[1][i] = data[1][n-i];
    }
  }
  else if (ads == 0) {
    for (i=0; i<n; i++) {
      bjh[0][i] = data[0][n-i];
      bjh[1][i] = data[1][n-i];
    }
  }

  //  for (i=0; i<n; i++)
  //printf("%.3f\t%.2f\n", bjh[0][i], bjh[1][i]);

  bjh_r = get_array(3, n);

  for (i=0; i<n; i++) {   // Several options for calculating pore size... see functions at the top
    bjh_r[0][i] = kjs_r(bjh[0][i]);
    bjh_r[1][i] = harkins_jura(bjh[0][i]); 
    bjh_r[2][i] = bjh_r[0][i]+bjh_r[1][i]+0.27;       // r_p = r_k + t
  }

  bjh_m = get_array(9, n);     // The big one
  
  for (i=1; i<n; i++) {
    bjh_m[0][i] = (bjh_r[0][i]+bjh_r[0][i-1])/2;               // <r_k>
    bjh_m[1][i] = (bjh_r[2][i]+bjh_r[2][i-1])/2;               // <r_p>
    bjh_m[2][i] = bjh_r[1][i-1]-bjh_r[1][i];                   // Dt
    bjh_m[3][i] = bjh[1][i-1]-bjh[1][i];                       // DV_g
    bjh_m[4][i] = bjh_m[3][i]*34.6/22400;                      // DV_l  (specific to N2!!)
    bjh_m[5][i] = (i==1)?0.0:(bjh_m[2][i]*bjh_m[8][i-1])/1e4;  // Dt*SS
    bjh_m[6][i] = (pow((bjh_m[1][i]/bjh_m[0][i]), 2)*(bjh_m[4][i]-bjh_m[5][i])); // Vp
    bjh_m[7][i] = 2*bjh_m[6][i]*1e4/bjh_m[1][i];               // S
    bjh_m[8][i] = (i==1)?bjh_m[7][i]:(bjh_m[8][i-1]+bjh_m[7][i]); // SS

    //printf("%.2e\t%.2e\t%.2e\t%.2e\t%.2e\t%.2e\t%.2e\t%.2e\t%.2e\n", bjh_m[0][i], bjh_m[1][i], bjh_m[2][i], bjh_m[3][i], bjh_m[4][i], bjh_m[5][i], bjh_m[6][i], bjh_m[7][i], bjh_m[8][i]);
  }

  psd = get_array(2, n-1);

  bjh_output = file_bjh_output(fname);
  
  v_p = 0.0;
  for (i=1; i<n-1; i++) {
    psd[0][i] = bjh_m[1][n-1-i];
    psd[1][i] = bjh_m[6][n-1-i];
    v_p += psd[1][i];
    fprintf (bjh_output, "%.4e\t%.4e\t%.4e\n", 2*psd[0][i], psd[1][i], v_p);
    //    printf ("%.4f\t%.4e\t%.4e\n", bjh[0][i], bjh_r[1][i], bjh_r[0][i]);

  }

  
  fclose(bjh_output);
  free(bjh);
  free(bjh_r);
  free(bjh_m);
  return 0;
  
}
